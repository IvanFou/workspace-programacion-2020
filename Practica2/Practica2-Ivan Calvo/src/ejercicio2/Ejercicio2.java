package ejercicio2;

import java.util.Scanner;

public class Ejercicio2 {

	public static void main(String[] args) {
	
		Scanner escaner = new Scanner(System.in);
		
		System.out.println("Dame un n�mero positivo");
		int contador = escaner.nextInt();
		
		int numero = 0; //Creamos variables vac�as fuera del bucle para poder usarlas luego dentro
		int suma = 0;
		
		for (int i = 0 ; i < contador ; i++) {
			System.out.println("Introduce un n�mero");
				numero = escaner.nextInt();
				suma = numero + suma; //hacemos que "suma" se sume a s� mismo cada n�mero que introduzcamos
				
				if (numero %2 == 0) {
					System.out.println("Es par");
				} else {
					System.out.println("Es impar");
				}
			}
		
		System.out.println("------------------------");
		System.out.println("El resultado es " + suma); 
		System.out.println("------------------------");
		
		
		
		escaner.close();
	}

}
