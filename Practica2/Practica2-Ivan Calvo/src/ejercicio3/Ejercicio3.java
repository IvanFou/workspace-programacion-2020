package ejercicio3;

import java.util.Scanner;

public class Ejercicio3 {

	public static void main(String[] args) {
		
		Scanner escaner = new Scanner(System.in);

		System.out.println("Dame una cadena para finalizar el siguiente bucle");
		String fin = escaner.nextLine();

		String cadena = "";
		int contador = 0; // Si que uso la cadena aunque ponga que no
		

		int contadorMas5 = 0;
		boolean contadormay = false; //Si que uso la cadena aunque ponga que no
		int contadorMay = 0;
		String cadenaLarga = "";

		do {

			System.out.println("Introduce una cadena");
			cadena = escaner.nextLine();
			contador++; // este contador cuenta las cadenas que se introducen hasta que se para el
						// programa

			if (cadena.length() < 5) {
				contadorMas5++; // contador para cadenas m�s largas de 5 caracteres
			}

			for (int i = 0; i < cadena.length(); i++) { // Contamos si tiene alguna may�scula
				char ch = cadena.charAt(i); // Si la tiene se suma el contador en +1

				if (ch >= 'A' && ch <= 'Z') {
					contadormay = true;
					contadorMay++;
				}
			}
			
			if (cadena.length() > cadenaLarga.length()) {
				cadenaLarga = cadena;  // Contamos la cadena m�s larga compr�ndola siempre con el valor de cadenaLarga(anterior)
			}

		} while (!cadena.equals(fin));

		System.out.println("----------------------------------------------------------");
		System.out.println("Hay " + contadorMas5 + " cadenas con m�s de 5 car�cteres");
		System.out.println("Hay " + contadorMay + " cadenas con al menos una may�scula");
		System.out.println("La cadena m�s larga es " + cadenaLarga);
		System.out.println("----------------------------------------------------------");
		escaner.close();
	}

}
