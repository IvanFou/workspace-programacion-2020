package ejercicio1;

import java.util.Scanner;

public class Ejercicio1 {

	public static void main(String[] args) {

		Scanner escaner = new Scanner(System.in);
		
		int option;
		
		do {
		
		System.out.println("************************");
		System.out.println("1- Contar may�sculas");
		System.out.println("2- Contrase�a correcta");
		System.out.println("3- Salir");
		System.out.println("************************");

		
		option = escaner.nextInt();
		

			switch (option) {
			case 1:
				System.out.println("Introduce una cadena");
				escaner.nextLine();
				String cadena = escaner.nextLine();

				int contador = 0;

				for (int i = 0; i < cadena.length(); i++) {  //Aqu� vamos posici�n por posici�n comprobando si tiene alguna letra 
					char ch = cadena.charAt(i);				// entre la "A" y la "Z"

					if (ch >= 'A' && ch <= 'Z') {
						contador++;
					}
				} 

				System.out.println("Hay: " + contador + " letras may�sculas");

				break;

			case 2:
				System.out.println("Escribe una contrase�a que contenga 1 vocal y no contenga ning�n n�mero");
				escaner.nextLine();
				String contrase�a = escaner.nextLine();

				contrase�a.toLowerCase();

				if (contrase�a.contains("a") || contrase�a.contains("e") || contrase�a.contains("i")
						|| contrase�a.contains("o") || contrase�a.contains("u")) {
					System.out.println("La contrase�a es correcta!");
				} else {
					System.out.println("La contrasela es incorrecta");
				}
				break;
			case 3:
				System.out.println("El programa se ha cerrado");
			default:
				break;
			}

		} while (option == 1 || option == 2);

		escaner.close();
	} 

}
