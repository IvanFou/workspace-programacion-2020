package ejercicio4;

import java.util.Scanner;

public class Ejercicio4 {

	public static void main(String[] args) {
		Scanner escaner = new Scanner(System.in);
		
		System.out.println("Introduce n�meros enteros, positivos o negativos");
		System.out.println("Para parar el programa escribe 'fin'");
		System.out.println("------------------------------------------------");
		System.out.println(" ");
		
		String cadena;
		int suma = 0;
		int mayor1 = 0;
		int mayor2 = 0;
		int entero = 0;
		
		do {
			System.out.println("Introduce un n�mero"); //Se repite correctamente 
														 //(Lo marco porque antes me dabaa fallos y solo se repet�a 3 veces, pero lo arregl�)
		
			cadena = escaner.nextLine();
			
			if (!cadena.equals("fin")) {
				entero = Integer.parseInt(cadena); //Si la cadena no contiene "fin", parseamos el String "cadena" a int "entero"
				suma = suma + entero;				   // para poder sumar su contenido en el int "suma"
			}
			
			if (entero > mayor1) { // Si "entero" es mayor que "mayor1", "mayor2" coger� el valor de "mayor1" y "mayor1" coger� el valor de "entero"
				mayor2 = mayor1;	// POR ESE ORDEN, es importante, si fuera al rev�s, mayor2 no tendr�a el valor del 2� numero mayor, sino del primero
				mayor1 = entero;
			} else if (mayor1 > entero && entero > mayor2){       //Si "mayor1" es mayor que "entero", 
																  //y "entero es menor que "mayor2",
				mayor2 = entero;								  // entonces "mayor2" coger� el valor de "entero"
				
			}
			
		} while (!cadena.contains("fin")); //Finaliza el bucle bien
		
		System.out.println("------------------------------------------");
		System.out.println("La suma de los numeros es: " + suma);
		if (mayor1 != 0) {  			
		System.out.println("El primer n�mero mayor introducido es: " + mayor1);
		} else {
			System.out.println("No se ha introducido ning�n n�mero");
		}
		if (mayor2 != 0) {
		System.out.println("El segundo n�mero mayor introducido es: " + mayor2);
		} else {
			System.out.println("Solo se ha introducido un n�mero, as� que no hay segundo mayor");
		}
		System.out.println("-------------------------------------------");
		
		
		escaner.close();
	}

}
