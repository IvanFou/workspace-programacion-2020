package ejercicio4; 

import java.util.Scanner;

public class Ejercicio4 {

	public static void main(String[] args) {
		
		Scanner escaner = new Scanner (System.in);
		System.out.println("Introduce un n�mero");
		int veces = escaner.nextInt();
		int numero;
		int suma = 0;
		
		for (int i = 0; i < veces ; i++) {
			System.out.println("Introduce otro n�mero");
			numero = escaner.nextInt();
			
			if(numero * 3 / 100 == 0) {
				System.out.println("Es m�ltiplo de 3");
			} else {
				System.out.println("No es m�ltiplo de 3");
			}
			suma = suma + numero;
		} 
		
		System.out.println("La suma total es: " + suma);
		
		escaner.close();
	}

}
