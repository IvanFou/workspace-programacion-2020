package ejercicio1;

import java.util.Scanner;

public class Ejercicio1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner escaner = new Scanner(System.in);
		
		System.out.println("Introduce la nota del alumno");
		double nota = escaner.nextDouble();
		double suma = 0;
		double media = 0;
		int contador = 0;
		
		do {
			System.out.println("Introduce otra nota");
			nota = escaner.nextDouble();
			suma = suma + nota;
			contador = contador + 1;
			
		} while(nota > 0);
		
		media = suma / contador;
		System.out.println("La nota media es: " + suma);
		
		escaner.close();
	}	

}
