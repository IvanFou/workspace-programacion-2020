package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner escaner = new Scanner (System.in);
		
		// Pedimos tama�o del vector
		int tam = 10;
		int[] enteros = new int[tam];
			
		
		//Rellenamos el vector pidiendo un valor para cada posici�n
		for (int i = 0 ; i < enteros.length ; i++) {
			System.out.println("Componente " + i + " del vector");
			enteros[i] = escaner.nextInt();
		}
		
		
		//Mostramos los valores del vector despu�s de introducirlos
		for (int i=0;i<enteros.length;i++) {
			System.out.println("La componente "+i+" del vector es "+enteros[i]);
		}
		escaner.close();
	}

}
