package ejercicio01;

import java.util.Scanner;

public class Resuelto {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int[] enteros;
		
		enteros = pide10Enteros();
		
		//Muestra los enteros al rev�s
		for (int i = enteros.length-1 ; i> 0 ; i--) {
			System.out.print(enteros[i] + " ");
		}
	}

	static int[] pide10Enteros() {
		Scanner escaner = new Scanner(System.in);
		
		int[] array = new int[10];
		
		//Rellena el Array
		for (int i = 0 ; i < array.length ; i++) {
			System.out.println("Dame un numero");
			array[i] = escaner.nextInt();
		}
		System.out.println("El array al rev�s es:");
		
		escaner.close();
		return array;
	}
}
