package ejercicio02;

public class Ejercicio02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int[] array = new int[10];
		
		for (int i = 0 ; i < array.length ; i++) {
			array[i] = (int)((Math.random() * (15 - 4) + 4));
		}
		
		mostrarArrayEnteros(array);
	}

	//Metodo para mostrar los datos del vector
	static void mostrarArrayEnteros(int[] enteros) {
		
		for (int i = 0 ; i < enteros.length ; i++) {
			System.out.println(enteros[i]);
		}
	}
	
	//Es lo mismo que el de arriba, pero con int, en vez de void
	static int[] mostrarArray(int[] llenos) {
		for (int i = 0 ; i < llenos.length ; i++) {
			System.out.println(llenos[i]);
		}
		return llenos;
	}

}
