package ejercicio01;

public class Metodos01 {

	public static String[] rellenarVector(String[] frases) {
		
		for (int i = 0; i < frases.length; i++) {
			System.out.println("Introduce una palabra para rellenar el hueco " + i + " del vector");
			frases[i] = Ejercicio01.escaner.nextLine();
		}
		
		return frases;
	}
	
	public static void cantidadPalabras(String[] frases) {
		
		int total = 0;
		for (int i = 0; i < frases.length; i++) {
			total = 0;
			if(!frases[i].equals("")){
				total += frases[i].split(" ").length;
			}
			System.out.println(total);
		}

	}
	
	public static void invertirVector(String[] frases) {
		System.out.println("El vector al rev�s es: ");
		for (int i = frases.length-1 ; i >= 0 ; i--) {
			System.out.print(frases[i] + " ");
		}
	}
}

