package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {

	static Scanner escaner = new Scanner (System.in);
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String[] vector = new String[5];

		System.out.println("Rellena el Vector");
		Metodos01.rellenarVector(vector);
		
		
		System.out.println("1-- Cantidad palabras");
		System.out.println("2-- Invertir Vector");
		System.out.println("3-- Salir");
		
		int option;
		do {
		option = escaner.nextInt();
		
		switch(option) {
		case 1:
			Metodos01.cantidadPalabras(vector);
			break;
		case 2:
			Metodos01.invertirVector(vector);
			break;
		case 3:
			System.out.println("El programa ha finalizado");
			break;
			
		default:
			System.out.println("Introduce una opci�n disponible");
			break;
		}
		} while(option > 3 || option < 0);
		
		escaner.close();
	}

}
