package ejercicio10;

import java.util.Scanner;

public class Ejercicio10 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner escaner = new Scanner(System.in);
		
		System.out.println("Dame 3 n�meros");
		int num1 = escaner.nextInt();
		int num2 = escaner.nextInt();
		int num3 = escaner.nextInt();
		
		if (num1 > num2 && num1 > num3) {		// We could also have done this with concatenated "if"
			System.out.println("El mayor es " + num1);  
		} else if (num2 > num1 && num2 > num3) {
			System.out.println("El mayor es " + num2);
		} else if (num3 > num2  && num3 > num1) {
			System.out.println("El mayor es " + num3);
		}
		
		
		escaner.close();
	}

}
