package ejercicio12;

import java.util.Scanner;

public class Ejercicio12 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner escaner = new Scanner(System.in);
		
		System.out.println("Dame un mes para saber cu�ntos d�as tiene, con la primera letra en may�scula");
		int mes = escaner.nextInt();
		
		if (mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 9 || mes == 11) {
			System.out.println("Este mes tiene 31 d�as");
		} else if (mes == 2) {
			System.out.println("Este mes tiene 28 d�as, 29 si es bisiesto");
		} else {
			System.out.println("Este mes tiene 30 d�as");
		}
		
		
		
		escaner.close();
	}

}
