package ejercicio07;

import java.util.Scanner;

public class Ejercicio07 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner escaner = new Scanner(System.in);
		
		System.out.println("Introduce un n�mero para saber su valor en la tabla ASCII"); // Just ask numbers between 32 and 256, not more or less
		int numero = escaner.nextInt();
		
		System.out.println((char)(numero));
		
		escaner.close();
	}

}
