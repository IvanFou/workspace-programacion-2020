package ejercicio09;

import java.util.Scanner;

public class Ejercicio09 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner escaner = new Scanner(System.in);
		
		System.out.println("Dame un n�mero");
		int num1 = escaner.nextInt();
		
		System.out.println("Dame otro n�mero");
		int num2 = escaner.nextInt();
		
		if (num1 > num2) {
			System.out.println("El primer n�mero es mayor que el segundo");
		} else if (num1 == num2) {
			System.out.println("Ambos n�meros son iguales");
		} else if (num1 < num2) {
			System.out.println("El segundo n�mero es mayor que el primero");
		}

		escaner.close();
	}

}
