package ejercicio11;

import java.util.Scanner;

public class Ejercicio11 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner escaner = new Scanner(System.in);
		
		System.out.println("Dame 3 n�meros");
		int hora = escaner.nextInt();
		int min = escaner.nextInt();
		int sec = escaner.nextInt();
		
		if (hora < 24 && hora > 0 && min < 60 && min > -1 && sec < 60 && sec > -1) {
			System.out.println("Es una hora acertada: " + hora + ":" + min + ":" + sec);
		} else {
			System.out.println("No es una hora v�lida");
		}
		
		
		escaner.close();
	}

}
