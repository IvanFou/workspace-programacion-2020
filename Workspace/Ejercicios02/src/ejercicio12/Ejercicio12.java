package ejercicio12;

import java.util.Scanner;

public class Ejercicio12 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner escaner = new Scanner(System.in);
		
		System.out.println("Dame una cadena de texto");
		String cadena = escaner.nextLine();
		
		System.out.println("Ahora dame un caracter");
		char caracter = escaner.nextLine().charAt(0);
		
		if(caracter == cadena.charAt(0)) {
			System.out.println("La cadena empieza por el car�cter");
		} else {
			System.out.println("Pues no empieza por el mismo car�cter");
		}
		
	
		
		escaner.close();
	}

}
