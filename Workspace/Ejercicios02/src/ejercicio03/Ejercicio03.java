package ejercicio03;

import java.util.Scanner;

public class Ejercicio03 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner escaner = new Scanner(System.in);
		
		System.out.println("Introduce el d�a (numero)");
		int dia = escaner.nextInt();
		
		// clean the buffer
		
		escaner.nextLine();
		
		System.out.println("Introduce el mes (texto)");
		String mes = escaner.nextLine();
		
		System.out.println("Introduce el a�o (numero)");
		int anno = escaner.nextInt();
		
		System.out.println(dia + "/" + mes + "/" + anno);
		
		escaner.close();
	}

}
