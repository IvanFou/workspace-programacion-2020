package ejercicio02;

import java.util.Scanner;

public class Ejercicio02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner escaner = new Scanner (System.in);
		
		System.out.println("Introduce 2 numeros");
		int num1 = escaner.nextInt();
		int num2 = escaner.nextInt();
		
		System.out.println(" ");
		System.out.println("El mayor es: ");
		System.out.println(Metodos.mayor(num1, num2));
		System.out.println("----------------");
		System.out.println("El menor es: ");
		System.out.println(Metodos.minimo(num1, num2));
		escaner.close();
	}

}
