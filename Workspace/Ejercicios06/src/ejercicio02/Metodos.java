package ejercicio02;

public class Metodos {

	static int mayor(int num1, int num2) {
		
		int mayor;
		
		if (num1 > num2) {
			mayor = num1;
		} else {
			mayor = num2;
		}
		return mayor;
	}
	
	static int minimo(int num1, int num2) {
		
		int menor;
		
		if (num1 < num2) {
			menor = num1;
		} else {
			menor = num2;
		}
		
		return menor;
	}
}
