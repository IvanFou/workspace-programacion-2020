package ejercicio04;

import java.util.Scanner;

public class Ejercicio04 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner escaner = new Scanner (System.in);
		
		System.out.println("Introduce un n�mero para saber si es primo o no");
		
		int numero = escaner.nextInt();
		
		boolean resultado = metodos.esPrimo(numero);
		
		if(resultado) {
			System.out.println("Es primo");
		} else {
			System.out.println("No es primo");
		}
		
		escaner.close();
	}

}
