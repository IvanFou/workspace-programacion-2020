package ejercicio06;

public class Ejercicio06 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("Tabla ascii: ");
		mostrarTablaAscii();
	}

	static void mostrarTablaAscii(){
		for (int i = 0 ; i < 256 ; i++) {
			System.out.println((char) i + " - " + i);
		}
	}
}
