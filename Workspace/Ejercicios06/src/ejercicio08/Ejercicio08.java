package ejercicio08;

import java.util.Scanner;

public class Ejercicio08 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner escaner = new Scanner (System.in);
		System.out.println("Numero Aleatorio: ");
		for(int i = 0 ; i < 100 ; i++) {
			System.out.println(aleatorio(25, 47));
		}
		
		escaner.close();
	}

	public static int aleatorio(int inicio, int fin) {
		
		int aleatorio = (int)(Math.random() * (fin - inicio + 1)) + inicio;
		
		return aleatorio;
	}
}
