package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner escaner = new Scanner (System.in);
		
		System.out.println("Dame una edad");
		int edadLeida=escaner.nextInt();
		
		boolean resultado = Metodos.esAdulto(edadLeida);
		
		if (resultado) {
			System.out.println("Es mayor de edad");
		} else {
			System.out.println("No es mayor de edad");
		}
		
		escaner.close();
	}
}
