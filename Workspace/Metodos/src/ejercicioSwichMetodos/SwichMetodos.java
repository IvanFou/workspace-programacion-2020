package ejercicioSwichMetodos;

import java.util.Scanner;

public class SwichMetodos {

	private static char[] suma;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner escaner = new Scanner(System.in);
		
		System.out.println("Calculadora");
		System.out.println("Escribe los n�meros");
		
		int x = escaner.nextInt();
		int y = escaner.nextInt();
		
		System.out.println("Indica la operaci�n");
		escaner.nextLine();
		char option = escaner.nextLine().charAt(0);
		
		switch(option){
		case '+':
			System.out.println(SwichMetodos.suma(x, y));
		break;
		
		case '-':
			System.out.println(SwichMetodos.resta(x , y));
		break;
		
		case '/':
			System.out.println(SwichMetodos.division(x, y));
		break;
		
		case '*':
			System.out.println(SwichMetodos.multiplicacion(x, y));
		break;
		}
		
		escaner.close();
	}
		
	public static double suma(int x, int y){
		return x + y;
	}
	
	public static double resta(int x, int y){
		return x - y;
	}
	
	public static double multiplicacion(int x, int y){
		return x / y;
	}
	
	public static int division(int x, int y){
		return x * y;
	}
	
}

