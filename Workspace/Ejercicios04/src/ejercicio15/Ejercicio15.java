package ejercicio15;

import java.util.Scanner;

public class Ejercicio15 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner escaner = new Scanner(System.in);
		
		System.out.println("Dame palabras");
		String cadena = escaner.nextLine();
		
		int posicionEspacio = cadena.indexOf(' ') + 1;
		String palabra1 = cadena.substring(0, posicionEspacio);
		String palabra2 = cadena.substring(posicionEspacio);
		
		 System.out.println(palabra2 + " " + palabra1);
		
		escaner.close();
	}

}
