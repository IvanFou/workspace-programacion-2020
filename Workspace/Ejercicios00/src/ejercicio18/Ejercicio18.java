package ejercicio18;

import java.util.Scanner;

public class Ejercicio18 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner escaner = new Scanner (System.in);
		
		System.out.println("Dame un entero");
		int entero = escaner.nextInt();
		System.out.println("El entero es " + entero);
		
		System.out.println("Dame un decimal");
		double doble = escaner.nextDouble();
		System.out.println("El decimal es " + doble);
		
		System.out.println("Dame un Byte");
		byte bite = escaner.nextByte();
		System.out.println("El Byte es " + bite);
		
		System.out.println("Dame un long");
		long largo = escaner.nextLong();
		System.out.println("El long es " + largo);
		
		System.out.println("Dame un short");
		short corto = escaner.nextShort();
		System.out.println("El short es " + corto);
		
		System.out.println("Dame un float");
		float flotante = escaner.nextFloat();
		System.out.println("El flotante es " + flotante);
		
		System.out.println("Dame un char");
		char caracter = escaner.next().charAt(0);
		System.out.println("El char es " + caracter);
		
		
		
		escaner.close();
	}

}
