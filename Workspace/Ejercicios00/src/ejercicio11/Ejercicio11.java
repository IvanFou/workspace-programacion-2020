package ejercicio11;

public class Ejercicio11 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		long long1 = 9223372036854775807L;
		long long2 = -9223372036854775807L;
		
		short short1 = 32767;
		short short2 = -32767;
		
		short s = (short) long1;
		short d = (short) long2;
		long f = (long) short1;
		long g = (long) short2;
		
		System.out.println(s);
		System.out.println(d);
		System.out.println(f);
		System.out.println(g);
	}

}
