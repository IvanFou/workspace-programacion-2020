package ejercicio15;

public class Ejercicio15 {

	static final String MENSAJE_INICIO = "Inicio del programa";
	static final String MENSAJE_FINAL = "Final del programa";
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String cadena = "Hola, me llamo juanito y vivo en colombia";
		String subcadena = cadena.substring(10, 30);

		System.out.println(MENSAJE_INICIO);
		System.out.println(cadena);
		System.out.println(subcadena);  			//This takes a piece of the String in a substring
		System.out.println(MENSAJE_FINAL);
		
		
	}

}
