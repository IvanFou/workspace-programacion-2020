package ejercicio14;

public class Ejercicio14 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String decimal = "4.5";
		double StDecimal = Double.parseDouble(decimal); // We are CASTING a String to a Double, so we can operate with it
		String largo = "29342893402";
		long StLong = Long.parseLong(largo); // We are CASTING a String to a Long, so we can operate with it
		String bite = "120";
		byte StBite = Byte.parseByte(bite); // We are CASTING a String to a Byte, so we can operate with it
		String entero = "450";
		int StEntero = Integer.parseInt(entero); // We are CASTING a String to a Integer, so we can operate with it
		
		//Now we show the results of the casts
		
		System.out.println(StDecimal + "Decimal");
		System.out.println(StLong + "Largo");
		System.out.println(StBite + "Byte");
		System.out.println(StEntero + "Entero");
	}

}
