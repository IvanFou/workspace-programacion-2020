package ejercicio10;

public class Ejercicio10 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int numero = 100;
		
		System.out.println(numero%5 == 0? "es multiplo de 5" : "no es multiplo de 5");
		System.out.println(numero%10 == 0? "es multiplo de 10" : "no es multiplo de 10");
		System.out.println(numero >=100? (numero == 100? "es igual a 100" : "es mayor que 100") : "es menor a 100");
		
	}

}
