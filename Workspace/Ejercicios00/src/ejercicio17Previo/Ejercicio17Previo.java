package ejercicio17Previo;

public class Ejercicio17Previo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String cadena1 = "Esto es una cadena";
		String cadena2 = "Hola caracola";
		String cadena3 = "Adios";
		String cadena4 = "Esto es otra cadena";
		String cadena5 = "Adios";
		
		System.out.println("Caena 1 " + cadena1);
		System.out.println("longitud : " + cadena1.length());
		System.out.println("Cadena 2 " + cadena2);
		System.out.println("longitud : " + cadena2.length());
		System.out.println("Cadena 3 " + cadena3);
		System.out.println("longitud : " + cadena3.length());
		System.out.println("Cadena 4 " + cadena4);
		System.out.println("longitud " + cadena4.length());
		
		//	resultado1 = cadena1.compareTo(cadena2)
		//  resultado1 >0 -> cadena1>cadena2
		

		int resultado1 = cadena1.compareTo(cadena2);
		int resultado2 = cadena1.compareTo(cadena3);
		int resultado3 = cadena2.compareTo(cadena3);
		int resultado4 = cadena1.compareTo(cadena4);
		int resultado5 = cadena3.compareTo(cadena5);
		
		System.out.println(resultado1);
		System.out.println(resultado2);
		System.out.println(resultado3);
		System.out.println(resultado4);
		System.out.println(resultado5);
		
	}

}
