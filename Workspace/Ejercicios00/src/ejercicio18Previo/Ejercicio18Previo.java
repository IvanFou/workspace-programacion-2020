package ejercicio18Previo;

import java.util.Scanner;

public class Ejercicio18Previo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner escaner = new Scanner (System.in);
		
		System.out.println("Dame un entero");
		int entero = escaner.nextInt(); // asking for Integer
		System.out.println(entero);
		
		System.out.println("Dame un doble"); // asking for Double
		double doble = escaner.nextDouble();
		System.out.println("Toma doble :3 " + doble);
		
		System.out.println(" ");
		System.out.println(" ");
		
		System.out.println("Hola, dinos c�mo te llamas"); // asking for name
		String cadena = escaner.next();
		System.out.println("Hola, me llamo " + cadena);
		escaner.close();

	}
}
